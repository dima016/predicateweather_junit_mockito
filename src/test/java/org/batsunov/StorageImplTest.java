package org.batsunov;

import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.*;

public class StorageImplTest {

    private StorageImpl storage = new StorageImpl();
    private InternalData internalData = new InternalData(LocalDate.of(2020,10,4),31,1);

    @Test
    public void shouldSaveAndReturnSameInfo() {
        storage.save(internalData);

        assertEquals(internalData,storage.load().get(0));
    }
}