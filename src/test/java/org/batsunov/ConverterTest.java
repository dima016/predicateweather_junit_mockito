package org.batsunov;

import org.junit.Test;

import java.time.ZoneId;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.Assert.assertEquals;


public class ConverterTest {

    private Converter converter = new Converter();
    private ProvidedData providedData = new ProvidedData((new GregorianCalendar(2020, Calendar.OCTOBER, 16).getTime()), 14.8f, 78.8f);
    private InternalData internalData;

    @Test
    public void shouldReturnCorrectInternalData() {
        internalData = new InternalData(providedData.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),//First arg
                (int) providedData.getTemperature(), //Second arg
                (int) providedData.getWetness());   //Third arg

        assertEquals(internalData, converter.convert(providedData));
    }

}