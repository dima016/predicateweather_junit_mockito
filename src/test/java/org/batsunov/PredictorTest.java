package org.batsunov;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;



@RunWith(MockitoJUnitRunner.class)
public class PredictorTest {

    List<InternalData> internalDataList;

    @Mock
    private Storage storage;

    private Predictor predictor;

    @Before
    public void setUp() throws Exception {//date=2020-10-16, temperature=14, wetness=78}
        predictor = new PredictorImpl(storage);
        internalDataList = new ArrayList<>();
        internalDataList.add(new InternalData(LocalDate.of(2020, 10, 16), 14, 78));
    }


    /**
     * Совет как тестировать такие классы:
     * <p>
     * Сделай отдельный package-private метод который будет тебе возвращать полностью отформатированную строку формата
     * На 2020-10-16 число  температура:14 влажность:78
     * Тогда можно будет применить проверку AssertEquals(expected, actual) и сравнивать ожидаемые строки и те что тебе возвращает метод
     * <p>
     * Таким образом ты протестируешь хотябы часть логики этого класса
     * <p>
     * Прийдется определить поведение для Storage чтобы он тебе возвращал какие-то данные
     */

    //Не понимаю,как сделать так,что бы проверять что возвращает приват метод-с лекции помню,что никак их не потестишь
    //значит нужно менять метод showPrediction что бы он возвращал String,то есть менять метод интерфейса,что бы возвращал стринг
    //а потом чтоли через циклы раскрывать предсказания в мейне,сомнительно
    //Был бы очень рад увидеть решение,как это правильно сделать

    @Test
    public void shouldReturnCorrectPredict() {

        String correctPredict = "На " + internalDataList.get(0).getDate() + " число температура:" + internalDataList.get(0).getTemperature() + " влажность:" + internalDataList.get(0).getWetness();


        assertEquals(correctPredict, predictor.correctFormat(internalDataList.get(0)));
    }


    @Test
    public void shouldShowPrediction() {


        predictor.showPrediction();

        Mockito.verify(storage).load();
    }
}