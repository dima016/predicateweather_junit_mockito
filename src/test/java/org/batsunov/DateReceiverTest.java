package org.batsunov;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;


import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DateReceiverTest {
    @Mock
    private DataProvider dataProvider;
    @Mock
    private Storage storage;
    @Mock
    private DataConverter converter;

    private List<ProvidedData> providedDataList;

    private DataReceiver testedInstance;

    // Лучше использовать конкретную дату LocalDate.of(year, month, day)
    private final InternalData internalData = new InternalData(LocalDate.of(2020,1,5), 7, 8);


    private final ProvidedData providedData = new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 10).getTime(), 20.7f, 35.6f);


    @Before
    public void setUp() {
        providedDataList = new ArrayList<>();
        testedInstance = new DataReceiverImpl(dataProvider, storage, converter);
        providedDataList.add(new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 10).getTime(), 20.7f, 35.6f));

        }


    @Test
    public void shouldCallSaveAndConvertAndGetList() {

        when(dataProvider.get()).thenReturn(providedDataList);
        when(converter.convert(providedData)).thenReturn(internalData);

        testedInstance.receive();


        Mockito.verify(storage).save(internalData);
    }


}