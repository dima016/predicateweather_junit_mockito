package org.batsunov;

public class DataReceiverImpl implements DataReceiver {

   private DataProvider dataProvider;
   private Storage storage;
   private DataConverter converter;

    public DataReceiverImpl(DataProvider dataProvider, Storage storage, DataConverter converter) {
        this.dataProvider = dataProvider;
        this.storage = storage;
        this.converter = converter;
    }

    @Override
    public void receive() {    //выгружает из DateProviderImpl in Storage

        for (ProvidedData el : dataProvider.get()) {
            storage.save(converter.convert(el));
        }

    }
}