package org.batsunov;

import java.util.List;

/**
 * Сохранения данных и извлечение их из абстрактного хранилища
 */
public interface Storage {
    /**
     * Сохранение записи
     */

    void save(InternalData internalData);

    /**
     * Должен возвращать все доступные данные
     */

    List<InternalData> load();

}