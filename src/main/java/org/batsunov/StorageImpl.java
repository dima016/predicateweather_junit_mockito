package org.batsunov;


import java.util.ArrayList;
import java.util.List;


public class StorageImpl implements Storage {

    // во-первых константы пишутся с большой буквы NEED_DAY
    // во-вторых, этой константе здесь не место
    // логика 7 дней должна полностью находится в классе PredictorImpl

    // Используй тип java.util.List в объявленной переменной
    private java.util.List<InternalData> internalDataList = new ArrayList<>();


    @Override
    public void save(InternalData internalData) {
        internalDataList.add(internalData);
    }


    // метод load должен возвращать ВСЕ данные (смотри интерфейс)
    @Override
    public List<InternalData> load() {
        return new ArrayList<>(internalDataList);
    }

}
