package org.batsunov;

import java.util.Date;
import java.util.Objects;

/**
 * Данные которые мы получаем из внешней системы. Требуют обработки
 */
public class ProvidedData {
    private Date date;
    private float temperature;
    private float wetness;

    public ProvidedData(Date date, float temperature, float wetness) {
        this.date = date;
        this.temperature = temperature;
        this.wetness = wetness;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public float getWetness() {
        return wetness;
    }

    public void setWetness(float wetness) {
        this.wetness = wetness;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProvidedData that = (ProvidedData) o;
        return Float.compare(that.temperature, temperature) == 0 &&
                Float.compare(that.wetness, wetness) == 0 &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, temperature, wetness);
    }

    @Override
    public String toString() {
        return "ProvidedData{" +
                "date=" + date +
                ", temperature=" + temperature +
                ", wetness=" + wetness +
                '}';
    }
}
