package org.batsunov;



public class Main {
    public static void main(String[] args) {
        Storage storage = new StorageImpl();
        Predictor predictor = new PredictorImpl(storage);
        DataReceiverImpl dateReceiver = new DataReceiverImpl(new DataProviderImpl(), storage, new Converter());

        dateReceiver.receive();



        predictor.showPrediction();
    }
}
