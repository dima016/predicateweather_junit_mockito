package org.batsunov;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Этот класс эмулирует реальные данные полуаемые от другого сервиса.
 *
 * Не нужно тестировать
 * (Не использовать в тестах!)
 */
public class DataProviderImpl implements DataProvider {

    @Override
    public List<ProvidedData> get() {
        return List.of(
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 10).getTime(), 20.7f, 35.6f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 11).getTime(), 19.2f, 50.8f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 12).getTime(), 20.0f, 55.3f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 13).getTime(), 17.4f, 45.1f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 14).getTime(), 16.5f, 35.5f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 15).getTime(), 16.2f, 25.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 16).getTime(), 14.8f, 78.8f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 17).getTime(), 16.9f, 90.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 18).getTime(), 15.6f, 100.0f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 19).getTime(), 14.7f, 68.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 20).getTime(), 13.2f, 46.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 21).getTime(), 12.7f, 67.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 22).getTime(), 11.0f, 19.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 23).getTime(), 13.3f, 12.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 24).getTime(), 10.2f, 35.2f),
                new ProvidedData(new GregorianCalendar(2020, Calendar.OCTOBER, 25).getTime(), 7.7f, 24.2f)
        );
    }
}
