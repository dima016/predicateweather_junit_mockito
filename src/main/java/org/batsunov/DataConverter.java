package org.batsunov;

/**
 * Объект служит для преобразования внешних данных во внутренние данные
 */
public interface DataConverter {
    InternalData convert(ProvidedData source);
}
