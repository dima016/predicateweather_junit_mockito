package org.batsunov;

import java.time.ZoneId;

public class Converter implements DataConverter {


    @Override
    public InternalData convert(ProvidedData source) {
        return new InternalData(source.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),//First arg
                (int) source.getTemperature(), //Second arg
                (int) source.getWetness());    //Third arg
    }
}
