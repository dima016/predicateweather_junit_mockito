package org.batsunov;

/**
 * Обьект этого типа должен получать данные у DataProvider и складывать их в хранилище Storage
 * Для преобразования типов использовать DataConverter
 */
public interface DataReceiver {
    void receive();
}
