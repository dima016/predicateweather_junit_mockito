package org.batsunov;

import java.time.LocalDate;


public class PredictorImpl implements Predictor {

    private Storage storage;


    public PredictorImpl(Storage storage) {
        this.storage = storage;
    }

    // Прогноз не должен строиться за даты в прошлом. То есть он должен начинаться с сегодняшнего дня. Другие даты должны быть отброшены
    @Override
    public void showPrediction() {

        int countDayOnShowList = 0;
        for (int i = 0; i < storage.load().size(); i++) {
            if (LocalDate.now().getDayOfMonth() <= storage.load().get(i).getDate().getDayOfMonth()) {
                while (countDayOnShowList != 7 && i + 1 <= storage.load().size()) {
                    System.out.println(correctFormat(storage.load().get(i++)));
                    countDayOnShowList++;
                }
            }
        }
    }

    @Override
    public String correctFormat(InternalData predict) {

        return "На " + predict.getDate() + " число температура:" + predict.getTemperature() + " влажность:" + predict.getWetness();
    }

}
